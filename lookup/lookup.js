const AWS = require('aws-sdk');
const {chunk} = require('lodash');

const dynamoDb = new AWS.DynamoDB();

const reservedDomains = ["www","docs"];
const TableName = "myblogdog";
const dummyFilePath = "FILEPATH-PLACEHOLDER";

exports.handler = async (event) => {
  const {domainMap} = JSON.parse(event.body);
  let validDomains = [];
  let foundDomain;
  for(const domains of chunk(Object.keys(domainMap),100)){
    const params = {
      RequestItems: {
       [TableName]: {
         Keys: domains.map(x=>({
           "DomainName":{
             S:x
           },
           "FilePath":{
             S:dummyFilePath,
           },
         })),
        }
      }
    };
    const resp = await dynamoDb.batchGetItem(params).promise();
    foundDomain = foundDomain || resp.Responses[TableName].length;
    validDomains = [...validDomains,...resp.Responses[TableName].filter(x=>x.GoogleFolderId.S === domainMap[x.DomainName.S]).map(x=>({
      domain: x.DomainName.S,
      googleFolderId:x.GoogleFolderId.S,
      template: x.Template && x.Template.S,
    }))];
  }
  return {
    statusCode:200,
    body: JSON.stringify({
      validDomains,
      foundDomain:!!foundDomain || Object.keys(domainMap).some(x=>reservedDomains.includes(x)),
    })
  }
};