const AWS = require('aws-sdk');
const {chunk} = require('lodash');

const dynamoDb = new AWS.DynamoDB();
const s3 = new AWS.S3();

const Bucket = "www.myblogdog.com";
const TableName = "myblogdog";

exports.handler = async (event) => {
  const {domain,googleFolderId} = JSON.parse(event.body);
  const domainsResp = await dynamoDb.query({
    ExpressionAttributeValues:{
      ":domainName":{
        S:domain,
      },
    },
    KeyConditionExpression: `DomainName = :domainName`,
    TableName,
  }).promise();
  const domainItems = domainsResp.Items.filter(x=>x.GoogleFolderId.S === googleFolderId);

  for(const domainItemsChunk of chunk(domainItems,25)){
    const params = {
      TransactItems:domainItemsChunk.map((x)=>({
        Delete:{
          Key: {
            "DomainName":{
              S: domain,
            },
            "FilePath": {
              S: x.FilePath.S
            }, 
          }, 
          TableName,
        }
      }))
    };
    await dynamoDb.transactWriteItems(params).promise()
  }
  
  const bucketItems = domainItems.filter(x=>x.FileHash)
  for(const bucketItemsChunk of chunk(bucketItems,1000)){
    const params = {
      Bucket,
      Delete: {
        Objects: bucketItemsChunk.map(x=>({
          Key: `${domain}/${x.FilePath.S}`
        }))
      }
    };
    await s3.deleteObjects(params).promise();
  }

  return {
    statusCode:200,
  }
};