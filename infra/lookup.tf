resource "aws_api_gateway_resource" "lookup" {
  path_part   = "lookup"
  parent_id   = aws_api_gateway_rest_api.myblogdog.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.myblogdog.id
}

resource "aws_api_gateway_method" "lookup" {
  rest_api_id   = aws_api_gateway_rest_api.myblogdog.id
  resource_id   = aws_api_gateway_resource.lookup.id
  http_method   = "POST"
  authorization = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_integration" "lookup" {
  rest_api_id             = aws_api_gateway_rest_api.myblogdog.id
  resource_id             = aws_api_gateway_resource.lookup.id
  http_method             = aws_api_gateway_method.lookup.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lookup.invoke_arn
}

data "archive_file" "lookup_zip" {
  type        = "zip"
  output_path = "${path.module}/tmp/lookup_zip.zip"
  source_dir = "${path.module}/../lookup"
}

resource "aws_lambda_function" "lookup" {
  filename         = "${data.archive_file.lookup_zip.output_path}"
  source_code_hash = "${data.archive_file.lookup_zip.output_base64sha256}"
  function_name = "myblodog-lookup"
  role          = aws_iam_role.lookup.arn
  handler       = "lookup.handler"
  runtime       = "nodejs12.x"
  publish = true
}

resource "aws_lambda_permission" "lookup" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lookup.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:242929592693:${aws_api_gateway_rest_api.myblogdog.id}/*/${aws_api_gateway_method.lookup.http_method}${aws_api_gateway_resource.lookup.path}"
}

resource "aws_iam_role" "lookup" {
  name = "myblogdog-lookup"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "lookup-dynamodb" {
  role       = "${aws_iam_role.lookup.name}"
  policy_arn = "${aws_iam_policy.dynamodb.arn}"
}
