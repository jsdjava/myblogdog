resource "aws_cloudfront_origin_access_identity" "myblogdog" {
}

resource "aws_cloudfront_distribution" "myblogdog" {
  origin {
    domain_name = aws_s3_bucket.myblogdog.bucket_regional_domain_name
    origin_id   = "s3"
    s3_origin_config {
      origin_access_identity =  aws_cloudfront_origin_access_identity.myblogdog.cloudfront_access_identity_path
    }
  }

  custom_error_response{
    error_caching_min_ttl = 0
    error_code = 404
    response_code = 404
    response_page_path = "/error.html"
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  aliases = ["*.myblogdog.com","myblogdog.com"]

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "s3"

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = aws_lambda_function.serve.qualified_arn
      include_body = false
    }
    
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 300
    default_ttl            = 300
    max_ttl                = 300
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.myblogdog.arn
    ssl_support_method = "sni-only"
  }

  price_class = "PriceClass_All"

}

data "archive_file" "serve_zip" {
  type        = "zip"
  output_path = "${path.module}/tmp/serve_zip.zip"
  source_file = "${path.module}/../serve/serve.js"
}

resource "aws_lambda_function" "serve" {
  filename         = "${data.archive_file.serve_zip.output_path}"
  source_code_hash = "${data.archive_file.serve_zip.output_base64sha256}"
  function_name = "myblogdog-serve"
  role          = aws_iam_role.edge_role.arn
  handler       = "serve.handler"
  runtime       = "nodejs12.x"
  publish = true
}

resource "aws_iam_role" "edge_role" {
  name = "myblogdog-edge"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource aws_iam_role_policy_attachment "edge-cloudwatch" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role       = "${aws_iam_role.edge_role.name}"
}

resource "aws_iam_role_policy_attachment" "edge-dynamodb" {
  role       = "${aws_iam_role.edge_role.name}"
  policy_arn = "${aws_iam_policy.dynamodb.arn}"
}

