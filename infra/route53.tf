resource "aws_route53_zone" "myblogdog" {
  name = "myblogdog.com"
}

resource "aws_route53_record" "myblogdog-validate" {
  zone_id         = aws_route53_zone.myblogdog.zone_id
  name = "${aws_acm_certificate.myblogdog.domain_validation_options.0.resource_record_name}"
  type = "${aws_acm_certificate.myblogdog.domain_validation_options.0.resource_record_type}"
  records = ["${aws_acm_certificate.myblogdog.domain_validation_options.0.resource_record_value}"]
  ttl = "300"
}

resource "aws_route53_record" "myblogdog-cloudfront" {
  zone_id         = aws_route53_zone.myblogdog.zone_id
  name = "*"
  type    = "CNAME"
  ttl = "5"
  records        = [aws_cloudfront_distribution.myblogdog.domain_name]
}

resource "aws_route53_record" "apex" {
  zone_id         = aws_route53_zone.myblogdog.zone_id
  name    = "myblogdog.com"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.myblogdog.domain_name
    zone_id                = aws_cloudfront_distribution.myblogdog.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "myblogdog" {
  allow_overwrite = true
  name            = "myblogdog.com"
  ttl             = 30
  type            = "NS"
  zone_id         = aws_route53_zone.myblogdog.zone_id

  records = [
    aws_route53_zone.myblogdog.name_servers[0],
    aws_route53_zone.myblogdog.name_servers[1],
    aws_route53_zone.myblogdog.name_servers[2],
    aws_route53_zone.myblogdog.name_servers[3],
  ]
}
