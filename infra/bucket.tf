resource "aws_s3_bucket" "myblogdog" {
  bucket = "www.myblogdog.com"
  acl    = "public-read"
  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  lifecycle_rule {
    id      = "latest"
    enabled = true
    
    tags = {
      "latest" = "true"
    }
    
    expiration {
      days = 1
    }
  }
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET","HEAD"]
    allowed_origins = ["https://*.myblogdog.com"]
  }
}

resource "aws_s3_bucket_policy" "public" {
  bucket = aws_s3_bucket.myblogdog.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicRead",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion"
            ],
            "Resource": "arn:aws:s3:::www.myblogdog.com/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "myblogdog-dump" {
  bucket = "myblogdog-dump"
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["https://www.myblogdog.com"]
  }
}