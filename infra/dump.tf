resource "aws_api_gateway_resource" "dump" {
  path_part   = "dump"
  parent_id   = aws_api_gateway_rest_api.myblogdog.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.myblogdog.id
}

resource "aws_api_gateway_method" "dump" {
  rest_api_id   = aws_api_gateway_rest_api.myblogdog.id
  resource_id   = aws_api_gateway_resource.dump.id
  http_method   = "POST"
  authorization = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_integration" "dump" {
  rest_api_id             = aws_api_gateway_rest_api.myblogdog.id
  resource_id             = aws_api_gateway_resource.dump.id
  http_method             = aws_api_gateway_method.dump.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.dump.invoke_arn

  request_parameters = {
    #"integration.request.header.file-id" = "method.request.header.file-id"
  }
}

resource "aws_lambda_permission" "dump" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.dump.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:242929592693:${aws_api_gateway_rest_api.myblogdog.id}/*/${aws_api_gateway_method.dump.http_method}${aws_api_gateway_resource.dump.path}"
}

data "archive_file" "dump_zip" {
  type        = "zip"
  output_path = "${path.module}/tmp/dump_zip.zip"
  source_dir = "${path.module}/../dump"
}

resource "aws_lambda_function" "dump" {
  filename         = "${data.archive_file.dump_zip.output_path}"
  source_code_hash = "${data.archive_file.dump_zip.output_base64sha256}"
  function_name = "myblogdog-dump"
  role          = aws_iam_role.lambda.arn
  handler       = "dump.handler"
  runtime       = "nodejs12.x"
}