resource "aws_api_gateway_resource" "register" {
  path_part   = "register"
  parent_id   = aws_api_gateway_rest_api.myblogdog.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.myblogdog.id
}

resource "aws_api_gateway_method" "register" {
  rest_api_id   = aws_api_gateway_rest_api.myblogdog.id
  resource_id   = aws_api_gateway_resource.register.id
  http_method   = "POST"
  authorization = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_integration" "register" {
  rest_api_id             = aws_api_gateway_rest_api.myblogdog.id
  resource_id             = aws_api_gateway_resource.register.id
  http_method             = aws_api_gateway_method.register.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.register.invoke_arn
}

data "archive_file" "register_zip" {
  type        = "zip"
  output_path = "${path.module}/tmp/register_zip.zip"
  source_file = "${path.module}/../register/register.js"
}

resource "aws_lambda_function" "register" {
  filename         = "${data.archive_file.register_zip.output_path}"
  source_code_hash = "${data.archive_file.register_zip.output_base64sha256}"
  function_name = "register"
  role          = aws_iam_role.register.arn
  handler       = "register.handler"
  runtime       = "nodejs12.x"
  publish = true
}

resource "aws_lambda_permission" "register" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.register.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:242929592693:${aws_api_gateway_rest_api.myblogdog.id}/*/${aws_api_gateway_method.register.http_method}${aws_api_gateway_resource.register.path}"
}


resource "aws_iam_role" "register" {
  name = "register"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "register-dynamodb" {
  role       = "${aws_iam_role.register.name}"
  policy_arn = "${aws_iam_policy.dynamodb.arn}"
}
