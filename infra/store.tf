resource "aws_api_gateway_rest_api" "myblogdog" {
  name = "myblogdog-api"
}

resource "aws_api_gateway_resource" "store" {
  path_part   = "store"
  parent_id   = aws_api_gateway_rest_api.myblogdog.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.myblogdog.id
}

resource "aws_api_gateway_method" "store" {
  rest_api_id   = aws_api_gateway_rest_api.myblogdog.id
  resource_id   = aws_api_gateway_resource.store.id
  http_method   = "POST"
  authorization = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_integration" "store" {
  rest_api_id             = aws_api_gateway_rest_api.myblogdog.id
  resource_id             = aws_api_gateway_resource.store.id
  http_method             = aws_api_gateway_method.store.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda.invoke_arn
  
  request_parameters = {
    #"integration.request.header.file-id" = "method.request.header.file-id"
    #"integration.request.header.file-name" = "method.request.header.file-name"
  }
}

resource "aws_api_gateway_deployment" "prod" {
  depends_on = [aws_api_gateway_integration.store,aws_api_gateway_integration.dump]
  rest_api_id = aws_api_gateway_rest_api.myblogdog.id
  stage_name  = "prod"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_api_key" "api_key" {
    name = "myblogdog-apikey"
    enabled = true
}

resource "aws_api_gateway_usage_plan" "usage_plan" {
    name         = "myblogdog-usage-plan"
    quota_settings {
        limit  = "1000000"
        period = "DAY"
    }
    throttle_settings {
        burst_limit = "1000"
        rate_limit  = "10000"
    }
    api_stages {
        api_id = aws_api_gateway_rest_api.myblogdog.id
        stage  = aws_api_gateway_deployment.prod.stage_name
    }
}

resource "aws_api_gateway_usage_plan_key" "usage_plan_key" {
    key_id        = "${aws_api_gateway_api_key.api_key.id}"
    key_type      = "API_KEY"
    usage_plan_id = "${aws_api_gateway_usage_plan.usage_plan.id}"
}

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:us-east-1:242929592693:${aws_api_gateway_rest_api.myblogdog.id}/*/${aws_api_gateway_method.store.http_method}${aws_api_gateway_resource.store.path}"
}

data "archive_file" "lambda_zip_inline" {
  type        = "zip"
  output_path = "${path.module}/tmp/lambda_zip_inline.zip"
  source_dir = "${path.module}/../store"
}

resource "aws_lambda_layer_version" "hugo_layer" {
  filename   = "${path.module}/../hugo-aws-lambda-layer/hugo.zip"
  layer_name = "hugo_layer"
  compatible_runtimes = ["nodejs12.x"]
}

data "archive_file" "hugo_template_layer_zip" {
  type        = "zip"
  output_path = "${path.module}/tmp/hugo_template_layer.zip"
  source_dir = "${path.module}/../template"
}

resource "aws_lambda_layer_version" "hugo_template_layer" {
  filename   = "${data.archive_file.hugo_template_layer_zip.output_path}"
  source_code_hash = "${data.archive_file.hugo_template_layer_zip.output_base64sha256}"
  layer_name = "hugo_template_layer"
  compatible_runtimes = ["nodejs12.x"]
}

resource "aws_lambda_function" "lambda" {
  filename         = "${data.archive_file.lambda_zip_inline.output_path}"
  source_code_hash = "${data.archive_file.lambda_zip_inline.output_base64sha256}"
  layers = [aws_lambda_layer_version.hugo_layer.arn,aws_lambda_layer_version.hugo_template_layer.arn]
  function_name = "myblogdog-store"
  role          = aws_iam_role.lambda.arn
  handler       = "store.handler"
  runtime       = "nodejs12.x"
  timeout = 180
  memory_size = 512
}

resource "aws_iam_role" "lambda" {
  name = "myblogdog-lambda"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "s3" {
  name = "myblogdog-s3-access"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": "arn:aws:s3:::*"
    }
  ]
} 
  EOF
}

resource "aws_iam_role_policy_attachment" "lambda-s3" {
  role       = "${aws_iam_role.lambda.name}"
  policy_arn = "${aws_iam_policy.s3.arn}"
}

resource "aws_iam_role_policy_attachment" "lambda-dynamodb" {
  role       = "${aws_iam_role.lambda.name}"
  policy_arn = "${aws_iam_policy.dynamodb.arn}"
}
