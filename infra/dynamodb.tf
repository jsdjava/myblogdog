resource "aws_dynamodb_table" "myblogdog" {
  name           = "myblogdog"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "DomainName"
  range_key      = "FilePath"

  attribute {
    name = "DomainName"
    type = "S"
  }

  attribute {
    name = "FilePath"
    type = "S"
  }
}

resource "aws_iam_policy" "dynamodb" {
  name = "lambda-dynamodb-access"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "dynamodb:*"
      ],
      "Resource": "${aws_dynamodb_table.myblogdog.arn}"
    }
  ]
} 
  EOF
}
