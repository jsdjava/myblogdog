resource "aws_api_gateway_resource" "delete" {
  path_part   = "delete"
  parent_id   = aws_api_gateway_rest_api.myblogdog.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.myblogdog.id
}

resource "aws_api_gateway_method" "delete" {
  rest_api_id   = aws_api_gateway_rest_api.myblogdog.id
  resource_id   = aws_api_gateway_resource.delete.id
  http_method   = "POST"
  authorization = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_integration" "delete" {
  rest_api_id             = aws_api_gateway_rest_api.myblogdog.id
  resource_id             = aws_api_gateway_resource.delete.id
  http_method             = aws_api_gateway_method.delete.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.delete.invoke_arn
}

resource "aws_lambda_permission" "delete" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.delete.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:us-east-1:242929592693:${aws_api_gateway_rest_api.myblogdog.id}/*/${aws_api_gateway_method.delete.http_method}${aws_api_gateway_resource.delete.path}"
}

data "archive_file" "delete_zip" {
  type        = "zip"
  output_path = "${path.module}/tmp/delete_zip.zip"
  source_dir = "${path.module}/../delete"
}

resource "aws_lambda_function" "delete" {
  filename         = "${data.archive_file.delete_zip.output_path}"
  source_code_hash = "${data.archive_file.delete_zip.output_base64sha256}"
  function_name = "myblogdog-delete"
  role          = aws_iam_role.lambda.arn
  handler       = "delete.handler"
  runtime       = "nodejs12.x"
  timeout = 30
}