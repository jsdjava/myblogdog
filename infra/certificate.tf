resource "aws_acm_certificate" "myblogdog" {
  domain_name = "myblogdog.com"
  subject_alternative_names = ["*.myblogdog.com"]
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" myblogdog {
  certificate_arn = "${aws_acm_certificate.myblogdog.arn}"
  validation_record_fqdns = [
    "${aws_route53_record.myblogdog-validate.fqdn}",
  ]
}