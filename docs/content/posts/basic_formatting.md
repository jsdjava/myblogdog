---
title: "Basic Formatting"
date: 2020-10-09T01:34:44-05:00
draft: true
---
### Basic Formatting
The formatting that you use inside a google doc (italics, bold, paragraphs, headings, etc) is 
first translated to markdown, and then to html. It will generally work in a WYSIWYG way.

#### Paragraphs
Paragraphs translate directly over, as shown below.
![Posts Paragraph](/images/postsParagraph.png)

#### Bold and Italics
Bold and italics translate over as well.
![Posts Bold and Italics](/images/postsBoldAndItalics.png)

#### Headings
Headings also translate directly over.
![Posts Headings](/images/postsHeadings.png)

#### Tables
Tables will also be converted to HTML.
![Posts Tables](/images/postsTable.png)

#### Tables with Inner Formatting
You can use bold/italics/other formatting options inside a table cell
![Posts Tables Inline](/images/postsTable1.png)

#### Lists
Numbered lists, ordered lists, and nested lists will also be translated to HTML.
![Posts Lists](/images/postsLists.png)
