---
title: "Emojis"
date: 2020-10-06T01:38:10-05:00
draft: true
---

Emojis can be used on your blog. [Emoji cheat sheet](https://www.webfx.com/tools/emoji-cheat-sheet/) is a good reference for emojis. Copy-paste from there to your google doc, and the emojis will render on your blog

![Posts Emojis](/images/postsEmojis.png)