---
title: "Images"
date: 2020-10-07T01:34:54-05:00
draft: true
---

Images can be used on your blog. Make sure you have the rights to the image and have properly attributed it, and then simply copy-paste it into your google doc. This image will then be rendered on your blog. Note that the size of the original image (not the size in the google doc) is what gets displayed on the blog, so you may want to downsize/rescale your image before pasting it into the doc.

![Posts Images](/images/postsImages.png)