---
title: "Advanced Formatting"
date: 2020-10-08T01:35:17-05:00
draft: true
---

### Advanced Formatting
You can insert other things into your blog posts like code blocks and block quotes as well.

#### Code Block
To place text into a code block, just use the `Courier New` font, and it will render as shown below. You can specify the language on the first line of the code block, just use `lang: <the language>`.
![Posts Code Block](/images/postsCodeBlock1.png)

#### Blockquote 
To create a blockquote, just put the content you want quoted inside quotation marks.
To add an author, just put a line inside the quotation marks with `—`before the author name.
![Posts Block Quote](/images/postsBlockQuotes.png)

#### Miscellaneous
There's a couple other miscellaneous elements that are supported, specifically superscripts, subscripts, highlighting, and keyboard directives. Superscripts and subscripts render in a WYSIWYG way. To highlight, just use the primary yellow color on any text you want highlighted. Keyboard directives can be created by just using the `Impact` font on the key sequence.
![Posts Extras](/images/postsExtras.png)
