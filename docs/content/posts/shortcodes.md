---
title: "Shortcodes"
date: 2020-10-05T01:35:00-05:00
draft: true
---
### Shortcodes
Shortcodes are a nice way for you to integrate concent like instagram posts, tweets, and youtube
videos with your blog.
All shortcodes can be enabled by using the primary green color to highlight the shortcode text

#### Instagram
To display an instagram post, use the text `instagram_simple` and then the post id and any options you want configured, so if the link to your post is `https://www.instagram.com/p/BGvuInzyFAe/`, then the instagram shortcode for it is `instagram_simple BGvuInzyFAe hidecaption`.
![Posts Instagram](/images/postsInstagram.png)

#### Youtube
To display a youtube video, use the text `youtube` and then the video id. If the link to your youtube video is `https://www.youtube.com/watch?v=ZJthWmvUzzc&feature=emb_title`, then the youtube shortcode for it is `youtube ZJthWmvUzzc`.
![Posts Youtube](/images/postsYoutube.png)

#### Twitter
To display a tweet, use the text `twitter_simple` and then the tweet id. If the link to your tweet is `https://twitter.com/DesignReviewed/status/1085870671291310081?ref_src=twsrc%5Etfw`, then the twitter shortcode for it is `twitter_simple 1085870671291310081`.
![Posts Twitter](/images/postsTwitter.png)

#### Vimeo
To display a vimeo video, use the text `vimeo_simple` and then the vimeo id. If the link to your vimeo video is `https://vimeo.com/4.8912912e+07`, then the vimeo shortcode for it is `vimeo_simple 48912912`.
![Posts Vimeo](/images/postsVimeo.png)
