---
title: "Editing Your Blog"
date: 2020-10-07T00:35:56-05:00
draft: true
---

To edit your blog, click the pencil icon.
![Editing your blog](/images/editingBlog1.png)

A google folder will be opened, containing all the content used to generate your blog.
![Editing your blog](/images/editingBlog2.png)

See the Posts and Sitemap chapters for more on how to make changes to your blog.
