---
title: "Updating Your Blog"
date: 2020-10-06T00:35:38-05:00
draft: true
---
To update your blog, just click the upload icon on the far left.
![Updating your blog](/images/updatingBlog1.png)

{{% notice note %}}
Updating can take a while (30-45s), depending on the base size of your blog and the number of changes you have made.
{{% /notice %}}

When the update is complete, click the preview icon in the center of your blog to view your
updated blog.
![Preview your blog](/images/updatingBlog2.png)

{{% notice note %}}
Your original blog domain will take about 5 minutes to sync up with your changes. Until then, you can always view your changes at the generated preview domain.
{{% /notice %}}

Your preview domain will match your domain, but will have some random words attached at the end of it. Every time you upload your website, you will get a new preview domain. Use the preview domain to see your changes in realtime before they hit your main blog.
![Preview domain](/images/updatingBlog3.png)