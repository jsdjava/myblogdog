---
title: "Creating Your First Blog"
date: 2020-10-08T00:35:22-05:00
draft: true
---
After launching the app, pick a name for your blog as well as a theme.
The available theme options are [clarity](https://clarity.myblogdog.com) and [newsroom](https://newsroom.myblogdog.com). Click the create button, then wait patiently as this can take a while.

![Creating a Blog](/images/creatingBlog1.png)

{{% notice note %}}
Blog names must be less than 50 characters long, and can only include lowercase alphanumerics and hyphens
{{% /notice %}}

Once its done creating, you should see a generic 404 page underneath your blog name. This is expected, as you still need to upload the website using the googl drive folder that has been created. Select the upload button at the far left, and wait again.
![Creation](/images/creatingBlog3.png)

{{% notice note %}}
The first upload of your blog is expected to take at least a minute, because it is building and pushing everything for the website. Subsequent updates should be quicker.
{{% /notice %}}

After uploading, your website will now be up and available at your requested domain. Select the link button (second to the far left) to view your website.
![Uploading](/images/creatingBlog5.png)
