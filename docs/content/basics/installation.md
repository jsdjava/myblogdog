---
title: "Installation"
date: 2020-10-10T10:10:53-05:00
draft: true
---

## Marketplace
Open the [myblogdog](https://gsuite.google.com/marketplace/app/streak_crm_for_g_suite/800057673271) app inside the Google Workspace Marketplace

## Install

Select the install button. Make sure to read the terms of service and privacy policy before clicking continue.

Sign in to your google account and review the permissions requested by the MyBlogDogApp before selecting the Allow option.