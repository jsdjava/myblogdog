---
title: "About"
date: 2020-10-08T01:20:00-05:00
draft: true
---

### About Sheet
The about sheet + page can be used to explain more about the author as well as the blog.
![About 1](/images/aboutNewsroom1.png)

#### About Title and Date
The about title is displayed at the top of the about page, and can be filled out as the Title property in the about sheet. The date is displayed below the title, and can also be filled out in the about sheet.
![About 2](/images/aboutNewsroom2.png)

#### About Post
In your google drive folder, there should be a document called About which links to the About sheet through the File row.
You can fill this out like you would a normal post (see the posts section for details), and it will render as the about page.
![About 3](/images/aboutNewsroom4.png)

#### About Page
The About Post renders as a normal html page as shown below.
![About 4](/images/aboutNewsroom3.png)
