---
title: "General"
date: 2020-10-09T01:19:51-05:00
draft: true
---
### Global Sheet
Using the global sheet, you can modify the title, posts per  page, and integrate disqus to enable
commenting.
![General 1](/images/generalNewsroom.png)

#### Title
The title is displayed at the top of the blog, and can be set in the Title row in the Global sheet for your Sitemap.
![General 2](/images/generalNewsroom1.png)

#### Title and Number of Blog Posts
The title also appears at the bottom of your blog, in the copyright notice. The number of blog posts that you set for the
`Number of Blog Posts` row will determine how many posts are displayed for each main page of the blog.
![General 3](/images/generalNewsroom2.png)

#### Disqus
Disqus is a pretty cool, easy integration that you can use to add interactivity through comments to your website. Create an account on disqus following the instructions [here](https://disqus.com/profile/signup/intent/). After getting your [shortname](https://help.disqus.com/en/articles/1717111-what-s-a-shortname), fill it out in the Global sheet, and you should have enabled comments on your website.
![General 4](/images/generalNewsroom4.png)
