---
title: "Post"
date: 2020-10-07T01:24:43-05:00
draft: true
---
### Posts
Posts are where you will be doing the majority of your blogging. Each Post corresponds to a unique webpage, with
unique properties like tags, categories, and a title, and you can set each of these and link posts using a Post sheet.
To link a new Post, simply copy an existing Post sheet in the SiteMap, then rename it to whatever you want that ends in the text `Post`. Then, set the title, date and description,draft,tags, and categories field.
To fill out the File row, create a new Google Doc, and use the name of it as the value for this row. This google doc is where you will put the actual content for that Post's webpage.
To fill out the thumbnail row, upload an image for the Post to the Google Drive, and use the name of the image file as the value for the thumbnail row.
![Post Newsroom 1](/images/postNewsroom1.png)

#### Post Doc
To make a post, you need to link a Google doc in the Post sheet you created, as described above.
In that Google Doc, you can use normal formatting, links, images, etc and they will render in the webpage in a WYSIWYG type of way. See the formatting chapter for more about this.
![Post Newsroom 2](/images/postNewsroom2.png)

#### Title and Date
The title and date directly correspond to the title of the post and the date it is posted at. The title appears at the top of a post, the date directly below it. Each can be set in the sheet you created for the Post.
![Post Newsroom 3](/images/postNewsroom3.png)

#### Post Page
The content of your post appears on the Post page, directly beneath the title and date.
![Post Newsroom 4](/images/postNewsroom4.png)

#### Tags Page
The tags you select for your post appear on the tags page. Selecting a tag will show the Posts linked to it. You can set tags as a comma separated list in the Post sheet for your Post in the Sitemap.
![Post Newsroom 5](/images/postNewsroom5.png)

#### Categories Page
The categories you select for your post appear on the categories page. Selecting a categtory will show the Posts linked to it. You can set categories as a comma separated list in the Post sheet for your Post in the Sitemap.
![Post Newsroom 6](/images/postNewsroom6.png)
