---
title: "Colors"
date: 2020-10-06T01:19:55-05:00
draft: true
---

### Colors Sheet
The colors sheet can be used to customize the colors on your website. Simply modify the fill color
for a specific row in the Colors sheet, and you will see the new color reflected in your website.
![Colors 1](/images/colorsNewsroom1.png)

#### Colors Normal Mode
You can see how the colors from the Colors sheet translate to different text and background colors in the newsroom blog.
![Colors 2](/images/colorsNewsroom2.png)

#### Colors Dark Mode
In dark mode, a completely different set of colors is used, which you can customize as well.
![Colors 3](/images/colorsNewsroom3.png)
