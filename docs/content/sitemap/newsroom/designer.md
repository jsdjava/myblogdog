---
title: "Designer"
date: 2020-10-05T01:20:07-05:00
draft: true
---
### Designer
The Designer sheet allows you to display your name and a link to a social media account of your choice.
![Designer Newsroom 1](/images/designerNewsroom1.png)

#### Designer Link
The Designer name and link appear at the bottom right of your blog, and can be set inside the Designer Sheet of your SiteMap.
![Designer Newsroom 2](/images/designerNewsroom2.png)
