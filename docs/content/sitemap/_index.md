+++
title = "Sitemap"
date = 2020-10-09T01:19:51-05:00
weight = 5
chapter = true
pre = "<b>2. </b>"
+++

To edit settings for your blog, open the Sitemap.xlsx file in your blog 
folder.
![Sitemap](/images/sitemap.png)

Pick the section for your theme for more specifics
 