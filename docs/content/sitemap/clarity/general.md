---
title: "General"
date: 2020-10-09T01:19:51-05:00
draft: true
---

### Global Sheet
Using the global sheet, you can modify the title, logo, author,
and other general settings for your blog.

![General Sitemap](/images/generalClarity.png)

#### Logo (Top) and Tags
Upload any image to your google drive folder, and then set the filename inside the Global sheet in order to add it as your logo. The logo will show up in the top left corner of your website.
Tags are used to group your posts. They show up in the Tags panel on the right of the website, and they can be set as a comma separated list inside the Global sheet.

![General Clarity 1](/images/generalClarity1.png)

#### Logo (Bottom) and Title 
Upload any image to your google drive folder, and then set the filename inside the Global sheet in order to add it as your logo. The logo will show up in the bottom left corner of your website. The title of your website is displayed at the bottom inside the copyright notice, and can be set inside the Global sheet as well.
![General Clarity 2](/images/generalClarity2.png)

#### Author, Author Description, and NavBar Position
Author and author description appear in the upper left side of the website, and can each be set respectively in the the Global Sheet. The NavBar appears for the mobile mode of the website and can be aligned to the left or right based on what you pick in the Global Sheet.
![General Clarity 3](/images/generalClarity3.png)

#### Disqus
Disqus is a pretty cool, easy integration that you can use to add interactivity through comments to your website. Create an account on disqus following the instructions [here](https://disqus.com/profile/signup/intent/). After getting your [shortname](https://help.disqus.com/en/articles/1717111-what-s-a-shortname), fill it out in the Global sheet, and you should have enabled comments on your website.
![General Clarity 4](/images/generalClarity4.png)
