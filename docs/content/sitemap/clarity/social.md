---
title: "Social"
date: 2020-10-05T01:20:07-05:00
draft: true
---
### Social
The social sheet in the SiteMap allows you to plug your various social media accounts. 
![Social Clarity 1](/images/socialClarity1.png)
#### Social Icons
None of the social fields are required, but the ones you fill out will appear as icons in the upper righthand corner of the website. Use normal http links as values in the sheet for them.
![Social Clarity 2](/images/socialClarity2.png)
#### RSS Feed
Enabling the RSS Feed will generate an RSS feed for your website, which you can view by clicking the rightmost "Broadcast" icon on the blog, after setting the RSS field to true in the social sheet.
![Social Clarity 3](/images/socialClarity3.png)