---
title: "Post"
date: 2020-10-07T01:24:43-05:00
draft: true
---
### Posts
Posts are where you will be doing the majority of your blogging. Each Post corresponds to a unique webpage, with
unique properties like tags, categories, and a title, and you can set each of these and link posts using a Post sheet.
To link a new Post, simply copy an existing Post sheet in the SiteMap, then rename it to whatever you want that ends in the text `Post`. Then, set the title, date and description,draft,
featured,tags, and categories field.
To fill out the File row, create a new Google Doc, and use the name of it as the value for this row. This google doc is where you will put the actual content for that Post's webpage.
To fill out the thumbnail row, upload an image for the Post to the Google Drive, and use the name of the image file as the value for the thumbnail row.
![Post Clarity 1](/images/postClarity1.png)

#### Title, Date, and Tags
The title, date and tags directly correspond to the title of the post, the date it is posted at, and the tags that it gets filtered along. The title appears at the top of a post, the date directly below it, and tags are placed next to the date in a list. Each can be set in the sheet you created for the Post.
![Post Clarity 2](/images/postClarity2.png)

#### Categories and Featured Post
Categories are another way to filter your posts, along with tags. They appear in the Categories Panel of the blog. If you set Featured Post to true in the spreadsheet, it will show the Post in the Featured Post section on the right hand side of the blog.
![Post Clarity 3](/images/postClarity3.png)

#### Post Page
The content of your post appears on the Post page, directly beneath the title and date.
![Post Clarity 4](/images/postClarity4.png)

#### Post Doc
To make a post, you need to link a Google doc in the Post sheet you created, as described above.
In that Google Doc, you can use normal formatting, links, images, etc and they will render in the webpage in a WYSIWYG type of way. See the formatting chapter for more about this.
![Post Clarity 5](/images/postClarity5.png)
