---
title: "About"
date: 2020-10-08T01:20:00-05:00
draft: true
---

### About Sheet
The about sheet + page can be used to explain more about the author as well as the blog.

![About 1](/images/aboutClarity1.png)

#### About Post
In your google drive folder, there should be a document called About which links to the About sheet through the File row.
You can fill this out like you would a normal post (see the posts section for details), and it will render as the about page.

![About 2](/images/aboutClarity2.png)

#### About Title and Date
The about title is displayed at the top of the about page, and can be filled out as the Title property in the about sheet. The date is displayed below the title, and can also be filled out in the about sheet.

![About 3](/images/aboutClarity3.png)
