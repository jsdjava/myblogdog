const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB();

const TableName = "myblogdog";
const dummyFilePath = "FILEPATH-PLACEHOLDER";

exports.handler = async (event) => {
  const {domain,googleFolderId,template} = JSON.parse(event.body);
  if(!(domain && googleFolderId && template)){
    return {
      statusCode:400
    }
  }
  const params = {
    Item: {
      "DomainName":{
        S: domain
      },
      "FilePath":{
        S: dummyFilePath,
      },
      "GoogleFolderId": {
        S: googleFolderId
      }, 
      "Template":{
        S: template,
      }
    }, 
    TableName,
    ConditionExpression: "attribute_not_exists(DomainName)",
  };
  await dynamoDb.putItem(params).promise();
  return {
    statusCode:200
  }
};
