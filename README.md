# Itsanovelidea
## Demo

[![Thumbnail](pics/myblogdog.gif)](https://drive.google.com/file/d/15I_F9AkKvLlXC2AY4gKLF0MTu4YdJ6gI/view?usp=sharing "Myblogdog - Click to open video")
[Click image (or here) to open video](https://drive.google.com/file/d/15I_F9AkKvLlXC2AY4gKLF0MTu4YdJ6gI/view?usp=sharing)

[Website](https://myblogdog.com/)

[Docs](https://docs.myblogdog.com/sitemap/)

## Description
Myblogdog is a google drive app that allows you to write blog posts using google docs, and then upload/sync it into an actual website.
It uses a api-gateway lambda architecture to compose the different pieces (uploading a blog, serving blog files off a subdomain, deleting blogs, etc). I also created a static website and some docs for myblogdog using some Hugo themes.

The different components in the project are:

Static Components:
* 404 - the static 404 html page served by myblogdog
* docs - Hugo docs website explaining how to use myblogdog
* site - Main website showing demos and examples of myblogdog as well as providing privacy/terms of use/contact pages

Components:
* delete - remove blogs from google drive as well as from their myblogdog subdomain
* dump - create an s3 signed url where zipped blog files can be uploaded
* lookup - determine if a blog name (subdomain) has been taken yet or not
* register - register a blog name (subdomain) for use and create it in google drive
* serve - route to different blogs (subdomains) from the main website
* store - upload/sync a blog
* gsuite - the Google Drive app (react frontend, google scripts backend) that talks to api gateway in order to create/sync/delete blogs

Infrastructure:
* infra - the cloudfront, apigateway, lambda, route53,dynamodb,s3 etc AWS services needed by the project

Tools/Wrappers
* hugo-aws-lambda-layer - I did not write this. It basically allows Hugo to run inside a lambda.
* template - I did not write these, they are the Newsroom and Clarity Hugo templates (slightly tweaked) that can be used for blog themes
* gd2md-html - This converts a google doc to html. I did not write this code, but I did tweak it to fix some issues

Major features completed are:
* Display created blogs using iframes
* Create blogs at selected subdomain (uploaded as websites with local google drive folders backing them)
* Delete blogs
* Preview blog changes
* Publish blog updates
* Fonts/bold/italics/sizes/etc converted from google doc to html
* Inline images converted from google doc to html
* Hyperlinks,emojis, various hugo shortcodes (like code highlighting, instagram embed, disqus) converted from google doc to html
* Custom colors for hugo themes
* Excel sheet that controls pages, page names, etc in the Hugo website
* Blockquotes/tables/lists converted from google doc to html
* Hugo theme features like tags, social media links, dark/light mode,categories, etc

## Running Locally
Pretty much impossible to run locally. The gsuite script can be built locally and loaded into google scripts,
but needs apikeys for the apigateway to really work.

## Attribution
* hotdog image came from https://freeiconshop.com/icon/hotdog-icon-outline-filled/
* hugo lambda layer came from https://github.com/jason-dour/hugo-aws-lambda-layer
* newsroom Hugo template (and any associated media) came from https://github.com/onweru/newsroom
* clarity Hugo template (and any associated media) came from https://github.com/chipzoller/hugo-clarity
* docs Hugo template (and any associated media) came from https://github.com/matcornic/hugo-theme-learn
* main website Hugo template (and any associated media) came from https://github.com/themefisher/vex-hugo
* google docs to html code came from https://github.com/evbacher/gd2md-html
