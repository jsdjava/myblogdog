const AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});

const dynamoDb = new AWS.DynamoDB();

const baseFilePath = "/tmp/template/public";
const previewMarker = "---";

exports.handler = async (event, context, callback) => {
  const {request} = event.Records[0].cf;
  
  // Match any '/' that occurs at the end of a URI. Replace it with a default index.html
  var olduri = request.uri;
  if (olduri.indexOf(".") === -1) {
    olduri = olduri + '\/'
  }
  olduri = olduri.replace(/\/\//, '\/');
  request.uri = olduri.replace(/\/$/, '\/index.html');

  const {uri,headers} = request;
  let [domain] = headers.host[0].value.split(".");
  
  // Rewrite requests from myblogdog.com -> www.myblogdog.com, basically
  // an ALIAS record
  if(domain==="myblogdog"){
    domain = "www";
  }

  if(domain.includes(previewMarker)){
    // its a preview url. if previewDomain + filePath is not in dynamodb, then redirect
    // to normal domain
    const [baseDomain] = domain.split(previewMarker);
    const [filePath] = uri.split("?");
    // lookup DomainName = previewDomain, FilePath = filePath in dynamodb
    try{
      (await dynamoDb.getItem({
        Key: {
          "DomainName": {
            S: domain
          }, 
          "FilePath": {
            S: `${baseFilePath}${filePath}`
          }
        }, 
        TableName: "myblogdog"
      }).promise()).Item.FileHash;
    }catch(err){

      // great, reroute to normal domain
      domain = baseDomain;
    }
  }
  request.uri = "/" + domain + request.uri;

  // Return to CloudFront
  return callback(null, request);
};