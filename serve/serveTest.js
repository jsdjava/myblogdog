const AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});

const dynamoDb = new AWS.DynamoDB();

const previewMarker = "---";

const uri = "indexy.html";
let domain = "asdf---friendly-loose-club";

async function serve(){
if(domain.includes(previewMarker)){
    // its a preview url. if previewDomain + filePath is not in dynamodb, then redirect
    // to normal domain
    const [baseDomain] = domain.split(previewMarker);
    const [filePath] = uri.split("?");
    // lookup DomainName = previewDomain, FilePath = filePath in dynamodb
    console.log(domain);
    try{
    const res = await dynamoDb.getItem({
        Key: {
        "DomainName": {
            S: domain
        }, 
        "FilePath": {
            S: `/tmp/template/public/${filePath}`
        }
        }, 
        TableName: "myblogdog"
    }).promise();
    res.Item.FileHash;
    console.log(res);
    }catch(err){
    console.log(err);
    // great, reroute to normal domain
    domain = baseDomain;
    }
    console.log(domain);
}
}
serve()
  .then(x=>console.log("done"))
  .catch(x=>console.log(x));