const fs = require('fs-extra');
const AWS = require('aws-sdk');
const AdmZip = require('adm-zip');
const mime = require('mime-types');
const randomWords = require('random-words');
const toml = require('@iarna/toml');
const yaml = require('js-yaml');
const glob = require('glob');
const shell = require('shelljs');
const md5File = require('md5-file')
const {merge,chunk} = require('lodash');

const s3 = new AWS.S3();
const dynamoDb = new AWS.DynamoDB();

const TableName = "myblogdog";
const Bucket = "www.myblogdog.com";
const dumpBucket = "myblogdog-dump";
const originalPath = "/opt";
const basePath = "/tmp/template";
const dummyFilePath = "FILEPATH-PLACEHOLDER"

const getDomainTemplate = async (domainName,googleFolderId)=>{
  const {Item} = await dynamoDb.getItem({
    Key: {
      "DomainName": {
        S: domainName,
      }, 
      "FilePath": {
        S: dummyFilePath
      }
    }, 
    TableName: "myblogdog"
  }).promise();
  return Item && Item.GoogleFolderId.S === googleFolderId && Item.Template.S;
}

const getFileHashes = async (domainName)=>{
  const resp = await dynamoDb.query({
    ExpressionAttributeValues:{
      ":domainName":{
        S:domainName,
      },
    },
    KeyConditionExpression: `DomainName = :domainName`,
    TableName,
  }).promise();
  return resp.Items.filter(x=>x.FileHash).reduce((acc,x)=>({
    ...acc,
    [x.FilePath.S]:x.FileHash.S,
  }),{});
}

const storeFileHashes = async (domainName,googleFolderId,fileMap)=>{
  for(const fileMapChunk of chunk(Object.entries(fileMap),25)){
    const params = {
      TransactItems:fileMapChunk.map(([k,v])=>({
        Put:{
          Item: {
            "DomainName":{
              S: domainName
            },
            "FilePath": {
              S: k
            }, 
            "FileHash":{
              S: v,
            },
            "GoogleFolderId":{
              S: googleFolderId
            }
          }, 
          TableName,
        }
      }))
    };
    await dynamoDb.transactWriteItems(params).promise()
  }
}

const storeWebsite = async (domainName,googleFolderId)=>{
  const previewDomain = `${domainName}---${randomWords(3).join("-")}`;
  shell.pushd(basePath);
  const {stdout,stderr,code} = shell.exec("hugo -D");
  shell.popd();
  const filePaths = glob.sync(`${basePath}/public/**/*.*`);
  const curFileHashes = await getFileHashes(domainName);
  const fileMap = filePaths.reduce((acc,x)=>{
    const hash = md5File.sync(x);
    if(hash === curFileHashes[x]){
      return acc;
    }
    return {
      ...acc,
      [x]:hash,
    };
  },{});
  await storeFileHashes(domainName,googleFolderId,fileMap);
  await storeFileHashes(previewDomain,googleFolderId,fileMap);
  for(const filePath of Object.keys(fileMap)){
    await s3.putObject({
      Body: fs.createReadStream(filePath),
      Key: `${domainName}/${filePath.split("/public/").slice(1).join("/")}`,
      Bucket,
      ContentType:mime.lookup(filePath) || "application/octet-stream",
    }).promise();
    await s3.putObject({
      Body: fs.createReadStream(filePath),
      Key: `${previewDomain}/${filePath.split("/public/").slice(1).join("/")}`,
      Bucket,
      ContentType:mime.lookup(filePath) || "application/octet-stream",
    }).promise();
  }
  return {filePaths,curFileHashes,fileMap,previewDomain,stdout,stderr};
}

const getDumpedZipFile = async (domain,googleFolderId)=>{
  const {Body} = await s3.getObject({
    Bucket:dumpBucket,
    Key:`${domain}-${googleFolderId}`,
  }).promise();
  return Body;
}

exports.handler = async (event) => {
  const {domain,googleFolderId} = JSON.parse(event.body);
  const template = await getDomainTemplate(domain,googleFolderId);
  if(!template){
    return {
      statusCode:404,
    }
  }
  const url = `https://${domain}.myblogdog.com`;
  fs.emptyDirSync(basePath);
  fs.copySync(`${originalPath}/${template}`,basePath);
  try{
    const mds = [];
    const dumpedZipFile = await getDumpedZipFile(domain,googleFolderId);
    const zip = new AdmZip(dumpedZipFile);
    const siteMap = JSON.parse(zip.getEntry("siteMap.json").getData().toString("utf8"));
    zip
      .getEntries()
      .filter(x=>[".jpg",".png",".gif"].find(y=>x.entryName.endsWith(y)))
      .forEach(x=>fs.writeFileSync(`${basePath}/static/${x.entryName}`,x.getData()));

    let baseSass;
    for(const [filePath,overrides] of Object.entries(siteMap)){
      if(filePath.endsWith(".toml")){
        const baseToml = toml.parse(fs.readFileSync(`${basePath}/${filePath}`));
        const mergedToml = merge({},baseToml,overrides,{
          baseUrl:"/",
          relativeURLs:true,
          canonifyURLs:false,
        });
        mds.push(toml.stringify(mergedToml))
        fs.writeFileSync(`${basePath}/${filePath}`,toml.stringify(mergedToml));
      } else if(filePath.endsWith(".yaml") || filePath.endsWith(".yml")){
        if(Array.isArray(overrides)){
          fs.writeFileSync(`${basePath}/${filePath}`,yaml.dump(overrides));
        }else{
          const baseYaml = yaml.safeLoad(fs.readFileSync(`${basePath}/${filePath}`,'utf8'));
          const mergedYaml = merge({},baseYaml,overrides);
          fs.writeFileSync(`${basePath}/${filePath}`,yaml.dump(mergedYaml));
        }
      } else if(filePath.endsWith(".sass")){
        baseSass = fs.readFileSync(`${basePath}/${filePath}`).toString("utf8");
        for(const [k,v] of Object.entries(overrides)){
          baseSass = baseSass.replace(new RegExp(`${k}:.*`),`${k}: ${v}`);
        }
        fs.writeFileSync(`${basePath}/${filePath}`,baseSass);
      }else{
        const post = zip.getEntry(`${overrides.file}.md`).getData().toString("utf8");
        const mergedPost = `+++
${toml.stringify(overrides)}
+++
        ${post}
        `;
        mds.push(mergedPost);
        fs.writeFileSync(`${basePath}/${filePath}`,mergedPost);
      }
    }
    const {previewDomain,filePaths,curFileHashes,fileMap,stdout,stderr} = await storeWebsite(domain,googleFolderId);
    return {
      statusCode: 200,
      body: JSON.stringify({
        url,
        previewUrl:`https://${previewDomain}.myblogdog.com`,
        filePaths,
        curFileHashes,
        fileMap,
        stdout,
        stderr,
        baseSass,
      })
    }
  } catch(e){
    return {
      body:e.message,
      statusCode:500,
    }
  }
};