export const onOpen = ()=> {
    DocumentApp.getUi()
      .createMenu('WordUp')
      .addItem('Open', 'showLanding')
      .addToUi();
  }

export const showLanding = ()=>{
  const html = HtmlService.createHtmlOutputFromFile('landing')
    .setTitle('WordUp')
  DocumentApp.getUi()
    .showSidebar(html);
}

export const showSidebar = () => {
  const html = HtmlService.createHtmlOutputFromFile('sidebar')
    .setTitle('WordUp')
  DocumentApp.getUi()
    .showSidebar(html);
}