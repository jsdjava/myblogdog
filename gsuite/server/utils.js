import {gdc,md} from './gdc.js';

const findFile = (directory,fileName)=>{
  return DriveApp.searchFiles(`title='${fileName.replace(/'/g,"\\'")}' and '${directory}' in parents`).next().getId();
}
  
const getMarkdown = (documentId)=> {
  const config = {
    htmlHeadings:false,
    zipImages:false,
    demoteHeadings:false,
    wrapHTML:false,
    renderHTMLTags:false,
    suppressInfo:true,
    documentId,
  }
  gdc.init(gdc.docTypes.md);
  const resp = md.doMarkdown(config);
  return {
    resp,
    images: gdc.images,
  };
}

function copyFolder(source, target) {
  var folders = source.getFolders();
  var files   = source.getFiles();

  while(files.hasNext()) {
    var file = files.next();
    file.makeCopy(file.getName(), target);
  }

  while(folders.hasNext()) {
    var subFolder = folders.next();
    var folderName = subFolder.getName();
    var targetFolder = target.createFolder(folderName);
    copyFolder(subFolder, targetFolder);
  }

}

  export {
    copyFolder,
    findFile,
    getMarkdown,
  }
