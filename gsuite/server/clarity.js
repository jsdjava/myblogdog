import {
  stringMap,
  arrayMap,
  boolMap,
  intMap,
  dateMap,
  leftRightMap,
  imageMap,
  docMap, 
  colorMap,
  optional, 
  socialMap,
  rssMap,
} from './configMaps.js';

const configs = [
    {
      source:"Global",
      dest:"config.toml",
      map:{
        "Title": stringMap("title"),
        "Author": [stringMap("author"),stringMap("params.author")],
        "Author description": stringMap("params.introDescription"),
        "Number of tags to show": intMap("params.numberOfTagsShown"),
        "Center logo?": stringMap("params.centerLogo"),
        "Logo": imageMap("params.logo"),
        "Mobile navbar position": leftRightMap("params.mobileNavigation"),
        "Disqus Shortname": optional(stringMap("disqusShortname")),
      }
    },
    {
      source:"Social",
      dest:"data/social.yaml",
      root:[],
      map:{
        "Github Link":socialMap("github"),
        "Twitter Link":socialMap("twitter"),
        "Linkedin Link":socialMap("linkedin"),
        "Youtube Link":socialMap("youtube"),
        "Facebook Link":socialMap("facebook"),
        "Instagram Link":socialMap("instagram"),
        "Enable RSS Feed":rssMap(),
      },
    },
    {
      source:"Colors",
      dest:"themes/hugo-clarity/assets/sass/_variables.sass",
      map:{
        "Nav Color":colorMap("\\$light"),
        "Nav Text Color":colorMap("\\$haze"),
        "Header Background Color":colorMap("\\$bg"),
        "Theme":colorMap("\\$theme"),
        "Table and Post Background":colorMap("\\$light_table_and_post_background"),
        "Light Mode Header Text":colorMap("\\$light_header_text"),
        "Light Mode Footer Background":colorMap("\\$light_footer_background"),
        "Light Mode Text":colorMap("\\$light_text"),
        "Light Mode Accent":colorMap("\\$light_accent"),
        "Dark Mode Header Text":colorMap("\\$dark_header_text"),
        "Dark Mode Text":colorMap("\\$dark_text"),
        "Dark Mode Footer Background":colorMap("\\$dark_footer_background"),
        "Dark Mode Accent":colorMap("\\$dark_accent"),
      }
    },
    {
      source: "About",
      dest: "content/about.md",
      map:{
        "Title":stringMap("title"),
        "Description":stringMap("description"),
        "Date":dateMap("date"),
        "File":docMap("file"),
      }
    },
    {
      folder:true,
      source:"Post",
      dest: "content/post/*.md",
      map:{
        Title:stringMap("title"),
        Date:dateMap("date"),
        Description:stringMap("description"),
        Draft:boolMap("draft"),
        Featured:boolMap("featured"),
        Tags:arrayMap("tags"),
        Categories:arrayMap("categories"),
        File: docMap("file"),
        Thumbnail: imageMap("thumbnail"),
      }
    }
];

export default configs;