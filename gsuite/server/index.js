import * as publicUiFunctions from './ui';
import * as publicApiFunctions from './api';

global.store = publicApiFunctions.store;
global.doGet = publicApiFunctions.doGet;
global.getDomainsMap = publicApiFunctions.getDomainsMap;
global.lookup = publicApiFunctions.lookup;
global.register = publicApiFunctions.register;
global.deleteDomain = publicApiFunctions.deleteDomain;
global.getTemplates = publicApiFunctions.getTemplates;
