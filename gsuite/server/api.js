import {uniq,isArray,merge} from "lodash";

import clarityConfig from "./clarity.js"
import newsroomConfig from "./newsroom.js";
import {findFile,getMarkdown,copyFolder} from "./utils.js";

const templateFolders = {
  "clarity":"1f8Q7FwphIknf812q3UTITY9ut8NFvrGq",
  "newsroom":"1yRD3sR9G8nWEn5jeHAeYm2cpxVFiA6T8",
};

const templateConfigs = {
  "clarity":clarityConfig,
  "newsroom":newsroomConfig,
};

const awsUrl = "https://4vuzhj0dte.execute-api.us-east-1.amazonaws.com/prod";
const awsApiKey = "REDACTED";
const myBlogDogLink = "MyBlogDog-Link-";

const getFolderId = domainName=>{
  const iter = DriveApp.searchFiles(`title = '${myBlogDogLink}${domainName}'`);
  const file = iter.next();
  return file.getParents().next().getId();
}

const getDomainErrors = (domain)=>{
  const validations = {
    "Domain length must be at least 1 character":domain.length>0,
    "Domain length must be less than 50 characters":domain.length<50,
    "Domain must contain only lowercase alphanumerics and hyphens":!/[^a-z0-9-]/.test(domain),
    "Domain must not contain consecutive hyphens":!/--/.test(domain),
  };
  return Object.entries(validations)
    .filter(([k,v])=>!v)
    .map(([k])=>k);
}

export const getTemplates = ()=> Object.keys(templateFolders);

export const lookup = (domain)=>{
  const domainErrors = getDomainErrors(domain);
  if(domainErrors.length){
    return domainErrors;
  }
  const resp = UrlFetchApp.fetch(`${awsUrl}/lookup`,{
    method : 'post',
    headers:{
      "x-api-key":awsApiKey,
    },
    contentType: 'application/json',
    payload: JSON.stringify({
      domainMap:{
        [domain]:"N/A"
      }
    })
  });
  return JSON.parse(resp.getContentText()).foundDomain ? ["Domain name is already in use"] :undefined;
}

export const deleteDomain = (domain)=>{
  const googleFolderId = getFolderId(domain);
  UrlFetchApp.fetch(`${awsUrl}/delete`,{
    headers:{
      "x-api-key":awsApiKey,
    },
    method : 'post',
    contentType: 'application/json',
    payload: JSON.stringify({
      domain,
      googleFolderId,
    })
  });
  DriveApp.getFolderById(googleFolderId).setTrashed(true);
}

export const getDomainsMap = ()=>{
  const iter = DriveApp.searchFiles(`title contains '${myBlogDogLink}'`);
  const domainMap = {};
  while(iter.hasNext()){
    const file = iter.next();
    const domain = file.getName().replace(myBlogDogLink,"");
    const googleFolderId = file.getParents().next().getId();
    domainMap[domain] = googleFolderId;
  }
  if(!Object.keys(domainMap).length){
    return [];
  }
  const resp = UrlFetchApp.fetch(`${awsUrl}/lookup`,{
    headers:{
      "x-api-key":awsApiKey,
    },
    method : 'post',
    contentType: 'application/json',
    payload: JSON.stringify({
      domainMap
    })
  });
  const {validDomains} = JSON.parse(resp.getContentText());
  return validDomains;
}

export const register = (domain,template)=>{
  const src = DriveApp.getFolderById(templateFolders[template])
  const dest = DriveApp.createFolder(domain);
  copyFolder(src,dest);
  dest.createFile(`${myBlogDogLink}${domain}`, "");
  UrlFetchApp.fetch(`${awsUrl}/register`,{
    headers:{
      "x-api-key":awsApiKey,
    },
    method : 'post',
    contentType: 'application/json',
    payload: JSON.stringify({
      domain,
      template,
      googleFolderId:dest.getId(),
    })
  });
}

export const store = (domain,template)=> {  
  let errors = [];
  let googleFolderId,siteMap;
  try{
    googleFolderId = getFolderId(domain);
  } catch(err){
    return {
      errors:[`Can't find folder for ${domain}`]
    }
  }
  console.log(googleFolderId)
  try{
    siteMap = SpreadsheetApp.openById(findFile(googleFolderId,"Site Map"));
  } catch(err){
    return {
      errors:["Can't find SiteMap excel sheet"]
    }
  }
  const sheets = siteMap.getSheets();
  const excelSheetsMap = sheets.reduce((acc,sheet)=>{
    const sheetValues = sheet.getDataRange().getValues();
    return {
      ...acc,
      [sheet.getName()]: sheetValues.slice(1).reduce((acc,row)=>({...acc,[row[0]]:row[1]}),{})
    }    
  },{});
  console.log(JSON.stringify(excelSheetsMap,null,2));
  const colorSheet = siteMap.getSheetByName("Colors");
  if(colorSheet){
    const colorsMap = colorSheet.getDataRange().getValues().reduce((acc,row,i)=>({
      ...acc,
      [row[0]]: colorSheet.getRange(i+1, 2).getBackground()
    }),{});
    excelSheetsMap["Colors"] = colorsMap;
  }
  let images = [];
  let docs = [];
  const overridesMap = {};
  for(let config of templateConfigs[template]){
    const sources = config.folder ? Object.keys(excelSheetsMap).filter(x=>x.includes(config.source)) : [config.source];
    for(let source of sources){
      if(!excelSheetsMap[source]){
        errors.push(`${source} sheet is missing.`)
        continue;
      }
      const dest = config.dest.replace("*",source);
      overridesMap[dest] = Object.entries(config.map).reduce((acc,[k,mapper])=>{
        const mappers = isArray(mapper) ? mapper : [mapper];
        return mappers.reduce((acc,mapper)=>{
          const {value,image,doc,validations} = mapper(excelSheetsMap[source][k],googleFolderId);
          errors = [...errors,...Object.entries(validations).filter(([k,v])=>!v).map(([k,v])=>k)];
          if(doc){
            docs.push(doc);
          }
          if(image){
            images.push(image);
          }
          if(value!==undefined){
            return isArray(acc) ? [...acc,value] : merge(acc,value);
          }
          return acc;
        },acc);
      },config.root || {});
    }
  }

  if(errors.length){
    return {
      errors,
    }
  }

  docs = uniq(docs);
  images = uniq(images);

  let blobs = [Utilities.newBlob(JSON.stringify(overridesMap,null,2),"txt","siteMap.json")];
  for(let image of images){
    const blob = DriveApp.getFileById(image).getBlob();
    const extension = blob.getName().split(".")[1];
    blob.setName(`${image}.${extension}`);
    blobs.push(blob);
  }
  for(let doc of docs){
    const{resp,images=[]} = getMarkdown(doc);
    blobs = [...blobs,...images,Utilities.newBlob(resp,"txt",`${doc}.md`)];
  }
  const zip = Utilities.zip(blobs);

  const dumpResponse = UrlFetchApp.fetch(`${awsUrl}/dump`,{
    method : 'post',
    contentType: 'application/json',
    headers:{
      "x-api-key":awsApiKey,
    },
    payload: JSON.stringify({
      domain,
      googleFolderId,
    })
  });
  const {url:dumpUrl} = JSON.parse(dumpResponse.getContentText());

  UrlFetchApp.fetch(dumpUrl,{
    method: 'put',
    contentType: "application/zip",
    payload: zip,
  });

  const resp = UrlFetchApp.fetch(`${awsUrl}/store`,{
    method : 'post',
    contentType: 'application/json',
    headers:{
      "x-api-key":awsApiKey,
    },
    payload: JSON.stringify({
      domain,
      googleFolderId,
    })
  });
  return JSON.parse(resp.getContentText());
}

export const doGet = ()=>{
  return HtmlService.createHtmlOutputFromFile("landing.html");
}