import {set} from 'lodash';
import moment from 'moment';

import {findFile} from './utils.js';

const optional = (fn)=>{
  return (val)=>{
    const resp = fn(val);
    if(!val){
      return {
        ...resp,
        value:undefined,
        validations:{},
      }
    }
    return resp;
  }
}

const stringMap = key=>(val="")=>{
  const value = set({},key,val);
  const validations = {
    [`${key} must not be empty`]:val,
    [`${key} must have less than 500 characters`]: val.length<500 
  };
  return { 
    value,
    validations,
  };
};

const arrayMap = key=>(val="")=>{
  const value = set({},key,val.split(","));
  const validations = {
    [`${key} must not be empty`]:val,
    [`All values in ${key} must not be empty`]:!val.split(",").find(x=>!x),
    [`All values in ${key} must have less than 500 characters`]:!val.split(",").find(x=>x.length>=500)
  }
  return {
    value,
    validations,
  }
}

const boolMap = key=>(val="")=>{
  const value = set({},key,val==="Yes");
  const validations = {
    [`${key} must be Yes or No`]: ["Yes","No"].includes(val)
  }
  return {
    value,
    validations,
  }
};

const intMap = key=>(val="")=>{
  val = parseInt(val);
  const value = set({},key,val);
  const validations = {
    [`${key} must be between 1 and 20`]: val >=1 && val<=20
  }
  return {
    value,
    validations,
  }
};

const dateMap = key =>(val="")=>{
  const value = set({},key,moment(val).format("YYYY-MM-DD"));
  const validations = {
    [`${key} must be a valid date`]: moment(val).isValid()
  }
  return {
    value,
    validations,
  }
}

const leftRightMap = key=>(val="")=>{
  const value = set({},key,val === 'Left' ? 'left' : 'right');
  const validations = {
    [`${key} must be Left or Right`]: ["Left","Right"].includes(val),
  }
  return {
    value,
    validations,
  }
}

const colorMap = key=>(val="")=>{
  const value = set({},key,val);
  const validations = {
    [`${key} must not be empty`]: val,
  };
  return {
    value,
    validations,
  }
}

const imageMap = key=>(val="",curDir)=>{
  const extension = [".png",".jpg",".gif"].find(x=>val.endsWith(x));
  let fileId;
  try{
    console.log("HELP ME1");
    console.log(curDir);
    console.log(val);
    fileId = findFile(curDir,val);
    console.log(fileId);
  } catch(e){
    // Its fine
    console.log(e);
  }
  const value = set({},key,`${fileId}${extension}`);
  const validations = {
    [`${key} must be an image file of type gif,jpg, or png`]: !fileId || extension,
    [`${key} must exist`]: fileId,
  }
  return {
    image: fileId,
    value,
    validations,
  }
};

const docMap = key=>(val="",curDir)=>{
  let fileId,type;
  try{
    console.log("HELP ME1")
    console.log(curDir);
    console.log(val);
    fileId = findFile(curDir,val);
    console.log(fileId);
    type = DriveApp.getFileById(fileId).getMimeType();
  } catch(err){
    console.log(err);
    // its fine
  }
  const value = set({},key,fileId);
  const validations = {
    [`${key} must not be empty`]: val,
    [`${key} must exist`]: !val || fileId,
    [`${key} must be a Google Doc`]: !type || type === "application/vnd.google-apps.document",
  };
  return {
    doc: fileId,
    value,
    validations,
  }
}

const socialMap = (key)=>(val="")=>{
  const value = val ? {
    item:key,
    url:val,
  } : undefined;
  return {
    value,
    validations:{},
  }
}

const rssMap = ()=>(val="")=>{
  const value = val === "Yes" ? {
    item: "rss",
    url:"index.xml",
    internal:true,
  }: undefined;
  const validations = {
    [`Enable Rss Feed must be Yes or No`]: ["Yes","No"].includes(val)
  }
  return {
    value,
    validations,
  } 
}

export {
  stringMap,
  arrayMap,
  boolMap,
  intMap,
  dateMap,
  leftRightMap,
  imageMap,
  docMap,
  colorMap,
  socialMap,
  rssMap,
  optional,
}
