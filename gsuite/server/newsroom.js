import {
    stringMap,
    arrayMap,
    boolMap,
    intMap,
    dateMap,
    imageMap,
    docMap,
    colorMap,
    optional,
  } from './configMaps.js';
  
  const configs = [
      {
        source:"Global",
        dest:"config.toml",
        map:{
          "Title": stringMap("title"),
          "Posts Per Page":[intMap("paginate"),intMap("params.paginate")],
          "Disqus Shortname": optional(stringMap("disqusShortname")),
        }
      },
      {
        source: "About",
        dest: "content/about.md",
        map:{
          "Title":stringMap("title"),
          "Date":dateMap("date"),
          "File":docMap("file"),
          "Image":imageMap("image"),
        }
      },
      {
        source: "Designer",
        dest:"data/designer.yml",
        map:{
          "Designer Name":stringMap("name"),
          "Designer Url":stringMap("url"),
        },
      },
      {
        source:"Colors",
        dest:"themes/newsroom/assets/sass/_variables.sass",
        map:{
          "Light Mode Posts Background":colorMap("\\$light_posts_background"),
          "Light Mode Background":colorMap("\\$light_background"),
          "Light Mode Text":colorMap("\\$light_text"),
          "Light Mode Accent":colorMap("\\$light_accent"),
          "Light Mode Theme":colorMap("\\$light_theme"),
          "Dark Mode Background":colorMap("\\$dark_background"),
          "Dark Mode Text":colorMap("\\$dark_text"),
          "Dark Mode Accent":colorMap("\\$dark_accent"),
          "Dark Mode Theme":colorMap("\\$dark_theme"),
          "Dark Mode Posts Background":colorMap("\\$dark_posts_background"),
        }
      },
      /*{
        source:"Social",
        dest:"data/social.yaml",
        map:{
          "Github":stringMap("github"),
          "Twitter":stringMap("twitter"),
          "LinkedIn":stringMap("linkedin"),
        }
      },*/
      {
        folder:true,
        source:"Post",
        dest: "content/post/*.md",
        map:{
          Title:stringMap("title"),
          Date:dateMap("date"),
          Description:stringMap("description"),
          Draft:optional(boolMap("draft")),
          Featured: optional(boolMap("featured")),
          Tags:optional(arrayMap("tags")),
          Categories:optional(arrayMap("categories")),
          File: docMap("file"),
          Image: imageMap("image"),
        }
      }
  ];
  
  export default configs;
  