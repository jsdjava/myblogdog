export default class GoogleLink extends React.Component {
    constructor(){
      super();
      this.state = {
        helpText:'',
      };
    }
    
    render() {
      const {link,children} = this.props;
      const {helpText} = this.state;
      return(
        <div className = "googleLink">
        <a href={link}>{children}</a>
        {helpText && <p>{helpText}</p>}
        </div>
      );
   }
}