import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import UpdateIcon from '@material-ui/icons/Update';
import LinkIcon from '@material-ui/icons/Link';
import VisibilityIcon from '@material-ui/icons/Visibility';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import Tooltip from '@material-ui/core/Tooltip';
import Iframe from 'react-iframe'
import LoadingOverlay from 'react-loading-overlay'
import PacmanLoader from 'react-spinners/PacmanLoader'
import {capitalize} from 'lodash';

import server from "../../utils/server";

class BlogPanel extends React.Component {
  constructor(){
    super();
    this.state = {};
  }
  render(){
    const {url,domain,folder,refreshDomains,template} = this.props;
    const {loading,previewUrl,errors} = this.state;
    return <Card>
    <LoadingOverlay active={loading}
      spinner={<PacmanLoader/>}>
      <CardContent>
        <h5 className="blogPanelHeader">{capitalize(domain)}</h5>
        { errors ? <div className = "blogPanelErrorsDiv">
          <IconButton size = "small" className="closeErrorButton" onClick = {()=>{
            this.setState({errors:undefined});
          }}>
            <CloseIcon />
          </IconButton>
          <h3> Errors </h3>
          <ul className="blogPanelErrors">{errors.map(x=><li>{x}</li>)}</ul>
          </div>:<Iframe url={previewUrl || url}/>}
      </CardContent>
      <CardActions className="blogPanelActions">
        <Tooltip title="Push">
        <IconButton onClick={async ()=>{
          this.setState({loading:true});
          try{
            const {previewUrl,errors,...resp} = await server.store(domain,template);
            console.log(resp);
            this.setState({
              errors,
              previewUrl,
            })
          } catch(err){
            console.log(err);
          }
          this.setState({loading:false})
          }}>
          <UpdateIcon aria-label="Update"/>
        </IconButton>
        </Tooltip>
        <Tooltip title="Link">
        <IconButton onClick={()=>{
          window.open(url,"_blank");
          }}>
          <LinkIcon aria-label="Link"/>
        </IconButton>
        </Tooltip>
        <Tooltip title="Preview">
        <IconButton disabled = {!previewUrl} onClick = {()=>{
          window.open(previewUrl,"_blank");
        }}>
          <VisibilityIcon aria-label="Preview"/>
        </IconButton>
        </Tooltip>
        <Tooltip title="Edit">
        <IconButton onClick = {()=>{
          window.open(`https://drive.google.com/drive/folders/${folder}`,"_blank");
        }}>
          <EditIcon aria-label="Edit"/>
        </IconButton>
        </Tooltip>
        <Tooltip title="Delete">
        <IconButton onClick={async ()=>{
          this.setState({loading:true});
          try{
            await server.deleteDomain(domain);
            refreshDomains();
          } catch(err){
            console.log(err);
          }
          this.setState({loading:false})
          }}>
          <DeleteIcon aria-label="Delete"/>
        </IconButton>
        </Tooltip>
      </CardActions>
    </LoadingOverlay>
  </Card>
  }
}

export default BlogPanel;