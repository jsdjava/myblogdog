import GoogleLink from "./GoogleLink";

export default class Footer extends React.Component {
  render() {
    return <div className="footer">
      <GoogleLink link={"https://www.myblogdog.com"}>Site</GoogleLink>
      <GoogleLink link={"https://docs.myblogdog.com"}>Docs</GoogleLink>
      <GoogleLink link={"https://www.myblogdog.com/terms-conditions"}>Legal</GoogleLink>
      <GoogleLink link={"https://www.myblogdog.com/contact"}>Contact</GoogleLink>
    </div>
  }
}