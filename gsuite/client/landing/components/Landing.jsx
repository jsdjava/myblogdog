import React from "react";

import Active from "./Active.jsx";
import Error from "./Error.jsx";
import Footer from "./Footer.jsx";
import "./App.css";

const AppStates = {
  ACTIVE:"active",
  ERROR:"error",
}

const backgroundImageUrl = "https://assets.myblogdog.com/backgroundImage.png";

class Landing extends React.Component{
  
  constructor(){
    super();
    this.state = {
      appState: AppStates.ACTIVE,
    }
  }

  render(){
    const {appState} = this.state;
    const getPage = ()=> {
      switch(appState){
        case AppStates.ACTIVE:
          return <Active/>;
        case AppStates.ERROR:
          return <Error/>
      }
    }
    return <div className="App" style={{ backgroundImage:`url(${backgroundImageUrl})` }}>
      <div className="blogsContainer">
        <h2> MyBlogDog </h2>
        {getPage()}
      </div>
      <Footer/>
    </div>
  }
}

export default Landing;
