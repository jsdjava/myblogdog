import Grid from '@material-ui/core/Grid';

import BlogPanel from "./BlogPanel.jsx";
import RegisterPanel from "./RegisterPanel.jsx";
import server from "../../utils/server";

export default class Active extends React.Component {

    constructor(){
      super();
      this.state = {};
      this.refreshDomains = this.refreshDomains.bind(this);
    }

    refreshDomains(){
      server.getDomainsMap()
        .then(resp=>{
          this.setState({
            validDomains:resp,
          })
        });
    }

    componentDidMount(){
      this.refreshDomains();
    }

    render() {
      const {validDomains} = this.state;
      if(!validDomains){
        return "Loading...";
      }
      return <Grid item xs={12}>
          <Grid container spacing={5}>
            <Grid key="registerPanel" item>
              <RegisterPanel refreshDomains={this.refreshDomains}/>
            </Grid>
            {validDomains.map((x) => (
              <Grid key={x.googleFolderId} item>
                <BlogPanel url={`https://${x.domain}.myblogdog.com`} 
                  domain={x.domain} 
                  folder={x.googleFolderId} 
                  template={x.template} 
                  refreshDomains={this.refreshDomains}/>
              </Grid>
            ))}
          </Grid>
        </Grid>;
    }
  }