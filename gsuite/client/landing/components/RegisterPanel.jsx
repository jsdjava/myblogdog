import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import LoadingOverlay from 'react-loading-overlay'
import TextField from '@material-ui/core/TextField';
import PacmanLoader from 'react-spinners/PacmanLoader';
import Button from '@material-ui/core/Button';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {debounce} from 'debounce';

import server from "../../utils/server";

class RegisterPanel extends React.Component {
  constructor(){
    super();
    this.lookup = debounce(this.lookup.bind(this),1000);
    this.register = this.register.bind(this);
    this.state = {
      templates:[],
    };
  }

  async componentDidMount(){
    const templates = await server.getTemplates();
    this.setState({
      templates,
      template:templates[0],
    });
  }

  async lookup(domain){
    let domainErrors = ["Could not lookup domain"];
    this.setState({
      loading:true,
      domain:undefined,
    });
    try{
      domainErrors = await server.lookup(domain);
    }catch(err){
      console.log("Could not lookup domain");
      console.log(err);
    }
    this.setState({
      errors: domainErrors,
      loading:false,
      domain,
    });
  }

  async register(){
    const {refreshDomains} = this.props;
    const {domain,template} = this.state;
    this.setState({
      loading:true,
    });
    try{
      await server.register(domain,template);
      refreshDomains();
    }catch(err){
      console.log("Could not register domain");
      console.log(err);
    }
    this.setState({
      domain:"",
      loading:false,
    })
  }

  render(){
    const {loading,domain,errors,templates,template} = this.state;
    return <Card>
    <LoadingOverlay active={loading}
      spinner={<PacmanLoader/>}>
      <CardContent>
        <div>
          <h3>Create Blog</h3>
          <TextField onChange={(e)=>this.lookup(e.target.value)}/>
          {domain ? (errors ? <CloseIcon/> : <CheckIcon/>) : null}
          <br/>
          <Select value={template} onChange={(event)=>{
            this.setState({
              template:event.target.value,
            });
          }}>
            {templates.map(x=><MenuItem value={x}>{x}</MenuItem>)}
          </Select>
          <ul>
            {(errors||[]).map(x=><li className="domainError">{x}</li>)}
          </ul>
        </div>
      </CardContent>
      <CardActions style={{height:"48px"}}>
          <Button disabled={errors || !domain ||!template} onClick={this.register}>
            Create
          </Button>
      </CardActions>
    </LoadingOverlay>
  </Card>
  }
}

export default RegisterPanel;