import GoogleLink from "./GoogleLink";

export default class Error extends React.Component {
  render() {
    return <div>
      Woops! Looks like something went wrong :^ (. Please refresh
      the page. If you have been incorrectly charged for something, please
      request a refund by emailing wordup9524@gmail.com. For other support, 
      please try the <GoogleLink url="https://www.myblogdog.com/help">help</GoogleLink> section.
    </div>
  }
}