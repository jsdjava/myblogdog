---
title: "Newsroom"
date: 2019-10-17T11:22:16+06:00
images: 
  - "images/newsroom-website.png"
  - "images/newsroom-drive.png"
  - "images/newsroom-map.png"
  - "images/newsroom-doc.png"
  - "images/newsroom-post.png"

# meta description
description : "Newsroom"

draft: false
---

[Newsroom](https://github.com/onweru/newsroom) is a clean looking blog with really nice blog post previews and animations. Checkout the [demo website](https://newsroom.myblogdog.com/) and the backing [google drive folder](https://drive.google.com/drive/folders/1tJsW2gXFS7b5zPCfF57_Jqkty9_b2sjt).
