---
title: "Clarity"
date: 2019-10-17T11:22:16+06:00
images: 
  - "images/clarity-website.png"
  - "images/clarity-drive.png"
  - "images/clarity-map.png"
  - "images/clarity-doc.png"
  - "images/clarity-post.png"

# meta description
description : "Clarity"
draft: false
---

[Clarity](https://github.com/chipzoller/hugo-clarity) is a slick looking blog theme featuring custom social media links, tags, categories, and an rss feed. Checkout the [demo website](https://clarity.myblogdog.com/) and the backing [google drive folder](https://drive.google.com/drive/folders/1OAQwbc9nrZULV7wIhd1cw-bZf3H5IgPR).
