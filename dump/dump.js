const AWS = require('aws-sdk');

const s3 = new AWS.S3();
const Bucket = "myblogdog-dump";

exports.handler = async (event) => {
  const {domain,googleFolderId} = JSON.parse(event.body);
  const url = await s3.getSignedUrlPromise('putObject', {
    Bucket,
    "Key": `${domain}-${googleFolderId}`,
    "ContentType": "application/zip"
  });
  return {
    statusCode:200,
    body: JSON.stringify({
      url,
    }),
  }
};